import Home from "./ShoppingCart/Home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
