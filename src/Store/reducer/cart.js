const initialState = {
  cartList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "SELECT_CART": {
      state.cartList = [...state.cartList];
      let foundIndex = state.cartList.findIndex((item) => {
        return item.id === action.payload.id;
      });

      if (foundIndex === -1) {
        state.cartList.push(action.payload);
      } else {
        state.cartList[foundIndex].quantity++;
      }
      return { ...state };
    }
    case "CHANGE_QUANTITY": {
      const { change, id } = action.payload;
      console.log(action);
      state.cartList = [...state.cartList];
      let foundIndex = state.cartList.findIndex((item) => {
        return item.id === id;
      });
      if (change) {
        state.cartList[foundIndex].quantity++;
      } else {
        if (state.cartList[foundIndex].quantity > 1) {
          state.cartList[foundIndex].quantity--;
        }
      }

      return { ...state };
    }
    case "REMOVE_CART": {
      const { id } = action.payload;
      console.log(action);
      state.cartList = [...state.cartList];

      state.cartList.splice(id, 1);

      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;
