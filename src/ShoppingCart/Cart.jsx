import React, { Component } from "react";
import { connect } from "react-redux";
class Cart extends Component {
  handleQuantityCart = (id, change) => {
    this.props.dispatch({
      type: "CHANGE_QUANTITY",
      payload: { id, change },
    });
  };

  handleRemoveCart = (id) => {
    this.props.dispatch({
      type: "REMOVE_CART",
      payload: id,
    });
  };

  renderCarts = () => {
    const { carts } = this.props;
    return carts.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>
            <img style={{ width: 100 }} src={item.img} />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => this.handleQuantityCart(item.id, false)}
              className="btn btn-secondary"
            >
              -
            </button>
            <span className="mx-2">{item.quantity}</span>
            <button
              onClick={() => this.handleQuantityCart(item.id, true)}
              className="btn btn-secondary"
            >
              +
            </button>
          </td>
          <td>{item.price.toLocaleString()}</td>
          <td>{(item.price * item.quantity).toLocaleString()}</td>
          <td>
            <button
              onClick={() => this.handleRemoveCart(item.id)}
              className="btn btn-danger"
            >
              Remove
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Cart Details</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Manipulate</th>
                  </tr>
                </thead>
                <tbody>{this.renderCarts()}</tbody>
                <tfoot>
                  <tr>
                    <td colSpan="5"></td>
                    <th>Total</th>
                    <td>
                      {this.props.carts
                        .reduce((total, item) => {
                          return (total += item.quantity * item.price);
                        }, 0)
                        .toLocaleString()}
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Buy
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProp = (state) => {
  return {
    carts: state.cart.cartList,
  };
};

export default connect(mapStateToProp)(Cart);
