import React, { Component } from "react";
import { connect } from "react-redux";
class ProductItem extends Component {
  handleSelectedDetail = (productItem) => {
    this.props.dispatch({
      type: "SELECT_DETAIL",
      payload: productItem,
    });
  };

  handleSelectedCart = (productItem) => {
    const carts = {
      id: productItem.id,
      name: productItem.name,
      price: productItem.price,
      img: productItem.img,
      quantity: 1,
    };
    this.props.dispatch({
      type: "SELECT_CART",
      payload: carts,
    });
  };

  render() {
    const { prod } = this.props;
    return (
      <div className="card">
        <img
          style={{ height: 250 }}
          className="card-img-top"
          src={prod.img}
          alt="mobile"
        />
        <div className="card-body">
          <div style={{ height: 170 }} className="card__content">
            <h4 className="card-title">{prod.name}</h4>
            <p className="card-text">{prod.desc}</p>
          </div>
          <button
            onClick={() => this.handleSelectedDetail(prod)}
            className="btn btn-secondary mx-4"
          >
            Detail
          </button>
          <button
            onClick={() => this.handleSelectedCart(prod)}
            className="btn btn-success"
          >
            Cart
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedDetail: state.product.selectedDetail,
    carts: state.cart.cartList,
  };
};

export default connect(mapStateToProps)(ProductItem);
