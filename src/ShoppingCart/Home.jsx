import React, { Component } from "react";
import Cart from "./Cart";
import Detail from "./Detail";
import ProductList from "./ProductList";
import { connect } from "react-redux";

class Home extends Component {
  render() {
    const { carts } = this.props;
    let totalQuantity = carts.reduce((total, item) => {
      return (total += item.quantity);
    }, 0);

    return (
      <div className="container">
        <div className="title d-flex py-4">
          <h3 className="text-danger">E-Shopping</h3>
          <button
            className="ml-auto bg-dark text-white"
            data-toggle="modal"
            data-target="#modelId"
          >
            <i className="fa fa-cart-arrow-down mr-2"></i>
            My Cart
            <span className="ml-2">({totalQuantity})</span>
          </button>
        </div>
        <ProductList />
        <h3 className="text-center py-5">Detail Infomation</h3>
        <Detail />
        <Cart />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    carts: state.cart.cartList,
  };
};

export default connect(mapStateToProps)(Home);
