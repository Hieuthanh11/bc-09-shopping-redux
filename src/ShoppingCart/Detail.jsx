import React, { Component } from "react";
import { connect } from "react-redux";
class Detail extends Component {
  render() {
    const { selectedDetail } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-5">
            <img style={{ width: 250 }} src={selectedDetail.img} alt="" />
          </div>
          <div className="col-7">
            <table className="table">
              <tbody>
                <tr>
                  <th>ID</th>
                  <td>{selectedDetail.id}</td>
                </tr>
                <tr>
                  <th>Name</th>
                  <td>{selectedDetail.name}</td>
                </tr>
                <tr>
                  <th>Price</th>
                  <td>{selectedDetail.price}</td>
                </tr>
                <tr>
                  <th>backCamera</th>
                  <td>{selectedDetail.backCamera}</td>
                </tr>
                <tr>
                  <th>frontCamera</th>
                  <td>{selectedDetail.frontCamera}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedDetail: state.product.selectedDetail,
  };
};

export default connect(mapStateToProps)(Detail);
