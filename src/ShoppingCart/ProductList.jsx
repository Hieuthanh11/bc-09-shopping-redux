import React, { Component } from "react";
import ProductItem from "./ProductItem";
import { connect } from "react-redux";
class ProductList extends Component {
  renderProduct = () => {
    const { products } = this.props;
    return products.map((item) => {
      return (
        <div key={item.id} className="col-3">
          <ProductItem prod={item} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">{this.renderProduct()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.product.productList,
  };
};

export default connect(mapStateToProps)(ProductList);
